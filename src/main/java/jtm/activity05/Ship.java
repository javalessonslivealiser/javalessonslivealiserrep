package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport {
    protected byte number_of_sails;
    public Ship(String id,byte number_of_sails) {
        super(id, 0,0);
        this.number_of_sails=number_of_sails;
    }
    public Ship(String id, float consumption, int tankSize) {
        super(id, consumption, tankSize);

    }
//comment
    @Override
    public String move(Road road) {
        if((road instanceof WaterRoad)){
            return this.getId()+ " Ship is sailing on "+ road.toString()+" with "+ this.number_of_sails+ " sails";
        }else{
        return "Cannot sail on "+ road.toString();
    }}
    public static void main(String[] args){
        Ship ship= new Ship("aaa", (byte) 3);
        System.out.println(ship.move(new WaterRoad()));
    }
}
