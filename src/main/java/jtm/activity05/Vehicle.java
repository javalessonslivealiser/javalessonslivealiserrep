package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {
    protected int number_of_wheels;

    /*public Vehicle(String id, float consumption, int tankSize) {
        super(id, consumption, tankSize);

    }*/
    //comment

    public Vehicle(String id, float consumption, int tankSize, int number_of_wheels) {
        super(id, consumption, tankSize);
        this.number_of_wheels = number_of_wheels;
    }


    @Override
    public String move(Road road) {
        if ((road instanceof WaterRoad)) {
            return "Cannot drive on " + road.toString();
        } else {
            String status = super.move(road);
            if (status.contains("moving"))
                status = status.replace("moving", "driving") + " with " + number_of_wheels + " wheels";
            else
                status=status;

            return status;
        }
    }
}
