package jtm.activity05;

import jtm.activity04.Road;

public class Amphibia extends Vehicle {
    private byte number_of_sails;
    public Amphibia(String id, float consumption, int tankSize, byte number_of_sails, int number_of_wheels) {
        super(id, consumption, tankSize, number_of_wheels);
        this.number_of_sails=number_of_sails;
    }
    @Override
    public String move(Road road){
        if (road instanceof WaterRoad)
            return super.getId()+" Amphibia"+" is sailing on "+ road.toString()+ " with "+ number_of_sails+" sails";
        else
            return super.move(road).replace("Vehicle","Amphibia");
    }
    public static void main(String[]args){
        Amphibia testAmp=new Amphibia("1",2.5f,20,(byte)4,5);
        System.out.println(testAmp);
    }
}
//comment
