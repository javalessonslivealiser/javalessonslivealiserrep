package jtm.activity06;

public class Human implements Humanoid {
    int stomach;
    int birthWeight=BirthWeight;
    boolean isAlive;

    public Human(){
        birthWeight=BirthWeight;
        isAlive=true;
    }

    @Override
    public void eat(Integer food) {
        stomach+=food;
    }

    @Override
    public Object vomit() {
        int tmp=stomach;
        stomach=0;
        return tmp;
    }

    @Override
    public String isAlive() {
        if (isAlive){
            return "Alive";
        }else{
            return "Dead";
        }
    }

    @Override
    public String killHimself() {
        isAlive=false;
        return "Dead";
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+" "+ (BirthWeight+stomach)+ " [";
    }
}
